#define _CRT_SECURE_NO_WARNINGS

#include "Match.h"
#include <stdio.h>
#include <Windows.h>

// 程序UI框架搭建
void PrintUI()
{
	printf("  足 球 比 赛 比 分 统 计\n");
	printf("============================\n");
	printf(" [1] 新建比赛  [2] 进行比赛\n");
	printf(" [3] 查询成绩  [4] 清空屏幕\n");
	printf(" [5] 保存数据  [0] 退出程序\n");
	printf("============================\n");
}

int main()
{
	// 定义变量
	LinkedList match = NULL;
	int input = 0;
	int confirm = 0;
	FILE* file = NULL;

	// 生成界面
	PrintUI();

	// 读取已有的比赛数据
	file = fopen("./Data/matchData.dat", "rb");
	if (file)
	{
		match = ReadData(file);
		if (match)
			printf("已成功加载数据！\n");
		else
			printf("数据加载失败！\n");
	}

	// 游戏逻辑
	do
	{
		printf("> ");
		scanf("%d", &input);
		while (getchar() != '\n') {}
		switch (input)
		{
		case 1:	// 新建
			if (match)
			{
			input_confirm:
				printf("您已经有一场比赛了，您确定要新建一场比赛并将原比赛覆盖吗？\n");
				printf("[1] 确定  [0] 返回  > ");
				scanf("%d", &confirm);
				while (getchar() != '\n') {}
				if (confirm == 0)
				{
					break;
				}
				else if (confirm != 1)
				{
					printf("输入错误，请重新输入！\n");
					goto input_confirm;
				}
			}
			match = (LinkedList)malloc(sizeof(LNode));
			if (!match)
			{
				printf("内存空间不足！\n");
				free(match);
				match = NULL;
				return -1;
			}
			match = GenerateMatch();;
			break;
		case 2: // 比赛
			GetMatchInfo(match);
			break;
		case 3:	// 查询
			if (!match)
			{
				printf("未找到比赛信息！\n");
				break;
			}
			PrintMatchInfo(match);
			break;
		case 4: // 清屏
			system("cls");
			PrintUI();
			break;
		case 5:	// 保存
			file = fopen("./Data/matchData.dat", "wb");
			SaveData(file, match);
			fclose(file);
			file = NULL;
			printf("保存成功！\n");
			break;
		case 0:	// 退出
			system("cls");
			printf("GOODBYE!\n");
			DestroyMatch(&match);
			break;
		default:
			break;
		}
	} while (input);

	return 0;
}