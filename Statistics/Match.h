#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#define MAX_STR_LEN 20
// 定义队伍
typedef struct Team
{
	char teamName[MAX_STR_LEN];	// 球队名字
	int matchCount;	// 比赛次数
	int score;	// 得分
	char detail[MAX_STR_LEN];	// 比赛得分详细
	int goalDifference;	// 净胜球
}Team;

// 定义比赛
typedef struct LNode
{
	Team data;
	struct LNode* next;
	struct LNode* prior;
}LNode, * LinkedList;

// 创建一支球队
Team* GenerateTeam(char name[]);

// 创建比赛
LinkedList GenerateMatch();

// 打印当前比赛信息
void PrintMatchInfo(LinkedList match);

// 获取比赛得分信息
void GetMatchInfo(LinkedList match);

// 清空比赛数据（释放内存）
void DestroyMatch(LinkedList* matchPtr);

// 比赛排名
void SortMatch(LinkedList match);

// 获取球队个数
int GetTeamCount(LinkedList match);

// 保存数据
void SaveData(FILE* file, LinkedList match);

// 读取数据
LinkedList ReadData(FILE* file);