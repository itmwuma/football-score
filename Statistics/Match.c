#define _CRT_SECURE_NO_WARNINGS
#include "Match.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ============== TOOLS ==============

// 交换结点的数据域
static void SwapData(LNode* node1, LNode* node2)
{
	Team tmp = { 0 };
	memcpy(&tmp, &node1->data, sizeof(Team));
	memcpy(&node1->data, &node2->data, sizeof(Team));
	memcpy(&node2->data, &tmp, sizeof(Team));
}

// 排序
static void Sort(LinkedList match, int(*cmp)(Team*, Team*))
{
	LNode* p = NULL;
	LNode* q = NULL;
	int isChanged = 1;

	if (!match) return;
	while (p != match->next && isChanged)
	{
		q = match->next;
		isChanged = 0;
		for (; q->next != match && q->next != p; q = q->next)
		{
			if (cmp(&q->data, &q->next->data) < 0)
			{
				SwapData(q, q->next);
				isChanged = 1;
			}
		}
		p = q;
	}
}

// 查找
static int SearchTeam(LinkedList match, char* teamName, LNode** targetTeam)
{
	LNode* p = match->next;
	while (p != match)
	{
		if (strcmp(p->data.teamName, teamName) == 0)
		{
			*targetTeam = p;
			return 1;
		}
		p = p->next;
	}
	*targetTeam = NULL;
	return 0;
}

// 获取队伍的细节信息
static void GetDetailInfo(Team* team, int* win, int* draw, int* fail)
{
	char* temp = strtok(team->detail, "/");
	*win = atoi(temp);
	*draw = atoi(strtok(NULL, "/"));
	*fail = atoi(strtok(NULL, "/"));
}

// 修改队伍的细节信息
static void ModifyDetailInfo(Team* team, int win, int draw, int fail)
{
	char temp[MAX_STR_LEN] = { 0 };
	char winStr[MAX_STR_LEN] = { 0 };
	char drawStr[MAX_STR_LEN] = { 0 };
	char failStr[MAX_STR_LEN] = { 0 };
	strcat(temp, _itoa(win, winStr, 10));
	strcat(temp, "/");
	strcat(temp, _itoa(draw, drawStr, 10));
	strcat(temp, "/");
	strcat(temp, _itoa(fail, failStr, 10));
	strcpy(team->detail, temp);
}

// ============== CALLBACK ==============

// 按得分排序 
static int SortByScore(Team* t1, Team* t2)
{
	return t1->score - t2->score;
}

// 按净胜球排序
static int SortByDifference(Team* t1, Team* t2)
{
	return t1->goalDifference - t2->goalDifference;
}

// ============== CORE ==============

// 创建球队
Team* GenerateTeam(char name[])
{
	Team* team = (Team*)malloc(sizeof(Team));
	if (!team) return NULL;
	strcpy(team->teamName, name);
	team->matchCount = 0;
	team->score = 0;
	team->goalDifference = 0;
	strcpy(team->detail, "0/0/0");
	return team;
}

// 创建比赛
LinkedList GenerateMatch()
{
	int teamCount = 0;
	char teamName[MAX_STR_LEN];
	Team* team = NULL;
	LNode* p = NULL;

	printf("===========================\n");

	// 获取队伍个数
input_teamCount:
	printf("请输入参加比赛的队伍个数：> ");
	scanf("%d", &teamCount);
	while (getchar() != '\n') {}
	if (teamCount <= 0)
	{
		printf("输入错误，请重新输入！\n");
		goto input_teamCount;
	}

	// 创建比赛
	LinkedList match = (LinkedList)malloc(sizeof(LNode));
	if (!match) return NULL;
	match->next = match;
	match->prior = match;
	for (int i = 0; i < teamCount; i++)
	{
		// 获取信息
		printf("请输入第%d支球队的名字：> ", i + 1);
		scanf("%s", teamName);
		while (getchar() != '\n') {}
		team = GenerateTeam(teamName);
		// 创建球队
		p = (LNode*)malloc(sizeof(LNode));
		if (!p)
		{
			free(match);
			return NULL;
		}
		memcpy(&p->data, team, sizeof(Team));
		// 添加球队
		p->next = match->next;
		match->next->prior = p;
		match->next = p;
		p->prior = match;
	}
	printf("录入成功！\n");
	printf("===========================\n");

	return match;
}

// 打印当前比赛信息
void PrintMatchInfo(LinkedList match)
{
	LNode* p = match->next;

	printf("===========================\n");

	printf("球队名字  比赛次数  得分  胜/平/输  净胜球\n");
	printf("-----------------------\n");
	while (p != match)
	{
		printf("%s  %d  %d  %s  %d\n",
			p->data.teamName,
			p->data.matchCount,
			p->data.score,
			p->data.detail,
			p->data.goalDifference);
		p = p->next;
	}

	printf("===========================\n");
}

// 获取比赛得分信息
void GetMatchInfo(LinkedList match)
{
	char gamerName1[MAX_STR_LEN];
	char gamerName2[MAX_STR_LEN];
	int winner = 0;
	int difference = 0;
	LNode* gamer1;
	LNode* gamer2;
	int gamerWin = 0;
	int gamerDraw = 0;
	int gamerFail = 0;

	printf("===========================\n");

	// 获取参赛双方
team_name_input:
	printf("请输入参赛双方[主队 客队]：> ");
	scanf("%s %s", gamerName1, gamerName2);
	while (getchar() != '\n') {}
	if (!SearchTeam(match, gamerName1, &gamer1) || !SearchTeam(match, gamerName2, &gamer2))
	{
		printf("球队输入错误，请重新输入！\n");
		goto team_name_input;
	}

	// 获取比分
winner_input:
	printf("请输入获胜者[主队1 / 客队2 / 平局3]：> ");
	scanf("%d", &winner);
	while (getchar() != '\n') {}
	if (winner != 1 && winner != 2 && winner != 3)
	{
		printf("输入错误，必须输入1或2！\n");
		goto winner_input;
	}
	if (winner != 3)
	{
	difference_input:
		printf("请输入净胜球数：> ");
		scanf("%d", &difference);
		while (getchar() != '\n') {}
		if (difference <= 0)
		{
			printf("输入错误，请重新输入！\n");
			goto difference_input;
		}
	}

	// 推算结果
	switch (winner)
	{
	case 1:	// 主队胜
		// 主队修改
		gamer1->data.score += 3;
		gamer1->data.matchCount++;
		gamer1->data.goalDifference += difference;
		GetDetailInfo(&gamer1->data, &gamerWin, &gamerDraw, &gamerFail);
		gamerWin++;
		ModifyDetailInfo(&gamer1->data, gamerWin, gamerDraw, gamerFail);
		// 客队修改
		gamer2->data.matchCount++;
		gamer2->data.goalDifference -= difference;
		GetDetailInfo(&gamer2->data, &gamerWin, &gamerDraw, &gamerFail);
		gamerFail++;
		ModifyDetailInfo(&gamer2->data, gamerWin, gamerDraw, gamerFail);
		break;
	case 2:	// 客队胜
		// 客队修改
		gamer2->data.score += 3;
		gamer2->data.matchCount++;
		gamer2->data.goalDifference += difference;
		GetDetailInfo(&gamer2->data, &gamerWin, &gamerDraw, &gamerFail);
		gamerWin++;
		ModifyDetailInfo(&gamer2->data, gamerWin, gamerDraw, gamerFail);
		// 主队修改
		gamer1->data.matchCount++;
		gamer1->data.goalDifference -= difference;
		GetDetailInfo(&gamer1->data, &gamerWin, &gamerDraw, &gamerFail);
		gamerFail++;
		ModifyDetailInfo(&gamer1->data, gamerWin, gamerDraw, gamerFail);
		break;
	case 3:	// 平局
		gamer1->data.score++;
		gamer2->data.score++;
		gamer1->data.matchCount++;
		gamer2->data.matchCount++;
		GetDetailInfo(&gamer1->data, &gamerWin, &gamerDraw, &gamerFail);
		gamerDraw++;
		ModifyDetailInfo(&gamer1->data, gamerWin, gamerDraw, gamerFail);
		GetDetailInfo(&gamer2->data, &gamerWin, &gamerDraw, &gamerFail);
		gamerDraw++;
		ModifyDetailInfo(&gamer2->data, gamerWin, gamerDraw, gamerFail);
		break;
	default:
		break;
	}

	// 重新排序
	SortMatch(match);

	printf("比赛结束！\n");

	printf("===========================\n");
}

// 清空比赛数据（释放内存）
void DestroyMatch(LinkedList* matchPtr)
{
	LinkedList match = *matchPtr;
	LNode* p = match->next;
	LNode* temp = NULL;
	while (p != match)
	{
		temp = p;
		p = p->next;
		//free(&temp->data);
		free(temp);
	}
	p = NULL;
	*matchPtr = NULL;
}

// 比赛排名
void SortMatch(LinkedList match)
{
	LNode* p = match->next;

	// 按照得分顺序排序
	Sort(match, SortByScore);

	// 对分数相等的球队进行净胜球排序
	while (p->next != match)
	{
		if (p->data.score == p->next->data.score
			&& p->data.goalDifference < p->next->data.goalDifference) 
			SwapData(p, p->next);
		p = p->next;
	}
}

// 获取球队个数
int GetTeamCount(LinkedList match)
{
	LNode* p = match->next;
	int count = 0;
	while (p != match)
	{
		count++;
		p = p->next;
	}
	return count;
}

// 保存数据
void SaveData(FILE* file, LinkedList match)
{
	if (!file || !match) return;

	// 创建存储数据
	int teamCount = GetTeamCount(match);
	if (teamCount <= 0) return;
	Team* teamData = (Team*)malloc(sizeof(Team) * teamCount);
	if (!teamData)  return;
	FILE* countFile = fopen("./Data/teamCount.dat", "wb");

	// 生成存储数据
	LNode* p = match->next;
	for (int i = 0; i < teamCount; i++, p = p->next)
	{
		memcpy(teamData + i, &p->data, sizeof(Team));
	}

	// 写入
	fwrite(teamData, sizeof(Team), teamCount, file);
	fwrite(&teamCount, sizeof(int), 1, countFile);
	fclose(countFile);

	// 释放内存
	free(teamData);
	teamData = NULL;
}


// 读取数据
LinkedList ReadData(FILE* file)
{
	if (!file) return NULL;

	// 创建临时存储数据
	FILE* countFile = fopen("./Data/teamCount.dat", "rb");
	if (!countFile)  return NULL;
	int teamCount = 0;
	LinkedList match = (LNode*)malloc(sizeof(LNode));
	if (!match) return NULL;
	match->next = match;
	match->prior = match;
	if (!match) return NULL;
	
	// 读取数据
	fread(&teamCount, sizeof(int), 1, countFile);
	if (teamCount <= 0) return NULL;
	Team* teamData = (Team*)malloc(sizeof(Team) * teamCount);
	if (!teamData) return NULL;
	fread(teamData, sizeof(Team), teamCount, file);
	
	// 还原数据为链表
	LNode* p = NULL;
	for (int i = 0; i < teamCount; i++)
	{
		p = (LNode*)malloc(sizeof(LNode));
		if (!p) return NULL;
		memcpy(&p->data, teamData + i, sizeof(Team));
		p->next = match->next;
		match->next->prior = p;
		p->prior = match;
		match->next = p;
	}

	// 排序
	SortMatch(match);

	// 释放内存
	free(teamData);
	teamData = NULL;

	return match;
}